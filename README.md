**This is a demo app designed with the fascinating and modern Google's Material Design.
It created using design support library to show how to implement material design in apps.
It covers many material design components, such as:**
- NavigationView
- CoordinatorLayout
- AppBarLayout
- CollapsingToolbarLayout
- NestedScrollView
- TabLayout
- FloatingActionButton
- SnackBar
- SwipeRefreshLayout
- RecyclerView
- CardView
- TextInputLayout
- BottomSheetDialog
- SharedElementTransitions
- BottomNavigationView
- Adaptive icon
- App shortcuts

![image](https://github.com/msddev/Android-MatrialDesignDemo/blob/master/pictures/1.png)

![image](https://github.com/msddev/Android-MatrialDesignDemo/blob/master/pictures/2.png)

![image](https://github.com/msddev/Android-MatrialDesignDemo/blob/master/pictures/3.png)

![image](https://github.com/msddev/Android-MatrialDesignDemo/blob/master/pictures/pad_1.png)

![image](https://github.com/msddev/Android-MatrialDesignDemo/blob/master/pictures/pad_2.png)
